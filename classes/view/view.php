<?php

    require_once $_SERVER["DOCUMENT_ROOT"]."/classes/model/sections.php";
    require_once $_SERVER["DOCUMENT_ROOT"]."/classes/model/products.php";
    require_once $_SERVER["DOCUMENT_ROOT"]."/classes/model/chars.php";
    require_once $_SERVER["DOCUMENT_ROOT"]."/classes/model/breadcrumbs.php";
    require_once $_SERVER["DOCUMENT_ROOT"]."/classes/model/cart.php";



    class View
    {



        function show_product_preview($product){
            echo '
                    
                    
                    
                    
                        <div class="item-preview" dataId="',$product[0],'">
                            <div class="item-preview-header"><a href="','/',$_SESSION['page-params'][2],'/',$product[0],'">',$product[1],'</a></div>
                            <div class="item-preview-content">
                                <div class="item-preview-pic"><a href="','/',$_SESSION['page-params'][2],'/',$product[0],'"><img src="',$product[3],'"></a></div>
                                <div class="item-preview-price button"><a href="','/',$_SESSION['page-params'][2],'/',$product[0],'">',$product[5],' ₽</a></div>
                                <div class="add-to-cart button" dataId = "',$product[0],'"><img src="/img/cart-small.png">Купить</div>
                            </div>
                        </div>
                    </a>
                    
                    
                    
                    
                    
                    
                ';


        }


        function show_section_20_items(){
            $page = $_GET['page'];
            if (!isset($page))
                $page = 1;
            $start = ($page - 1) * 20;
            $products = Products::get_20_products($_SESSION['page-params'][1],$start);
            foreach ($products as $value){
                self::show_product_preview($value);
            }

        }

        function show_section_name($id){
            $res = DBQuery::query("SELECT `name` FROM `sections` where `id` = $id");
            if (!$res){
                $res = false;
            }
            echo mb_strtolower($res[0][0]);
        }


        function show_similar_items(){
            $products = Products::get_similar_products($_SESSION['page-params'][1]);
            foreach ($products as $value){
                self::show_product_preview($value);
            }
        }

        function show_product($id){
            $product = (Products::get_product($id));
            echo '
            
            
           
                <div class="main-header">',
                $product[1],'    
                </div>
                <div class="main-content">
                    <div class="d-flex justify-content-between item-main">
                        <div class="col-4 item-pic">
                            <img src="',$product[3],'">
                        </div>
                        <div class="col-8 item-info">
                            <div class="item-price">Цена: <span class="item-price-value">',$product[5],' ₽</span></div>
                            <div class="item-text">',$product[4],'</div>
                            <div class="item-amount d-flex justify-content-start">
                                <div class="minus">-</div>
                                <input type="text" id="amount" name="amount" class="amount" min=1 value="1">
                                <div class="plus">+</div>
                            </div>
                            <div class="add-to-cart button" dataId="',$product[0],'"><img src="/img/cart-small.png">В корзину</div>
                        </div>
                    </div>
                    
                    <div class="main-header">
                        Характеристики  
                    </div>
                    <div class="chars">
                        <div class="char">
                            <div class="char_name">
                                ',$product[6][0],'
                            </div>
                            <div class="w-100" style="position: relative;"><div class="char-line"></div></div>
                            <div class="char-value uppercase">
                                ',$product[6][1],'
                            </div>
                        </div>
                        <div class="char">
                            <div class="char_name">
                                ',$product[7][0],'
                            </div>
                            <div class="w-100" style="position: relative;"><div class="char-line"></div></div>
                            <div class="char-value lowercase">
                                ',$product[7][1],'
                            </div>
                        </div>
                        
                    </div>
                    
                    
                    
                    <div class="main-header">
                        Похожие товары
                    </div>
                    <div class="similar-items d-flex justify-content-between flex-wrap">',
                        self::show_similar_items(),'
                    </div>
                </div>
                
                
                
            
            
            
            
            ';
            //var_dump($product);
            //var_dump(Products::get_product($id)[0]);
        }

        function show_breadcrumbs(){
            $crumbs = Breadcrumbs::get_breadcrumbs();
            //var_dump($crumbs);
            if (!empty($crumbs)) {
                echo '<ul class = "breadcrumbs d-flex justify-content-start"';
                foreach ($crumbs as $value) {
                    if (!empty($value['url']))
                        echo '<li>
                                  <a href="',$value['url'],'">',$value['text'],'</a>
                                  <span>&nbsp>&nbsp</span>
                              </li>
                            ';
                    else
                        echo '<li>
                                  ',$value['text'],'
                              </li>
                            ';
                }
                echo '</ul>';
            }
            //var_dump($crumbs);
        }

        function show_pagination($section_id){
            $products_per_page = 20;
            $page = $_GET['page'];
            if (!isset($page))
                $page = 1;
            $count_products = Products::get_count_products($section_id);
            $count_pages = ceil ($count_products / $products_per_page);

            echo '<div class="pagination">';

            echo '<a class="page-button prev';
            if ($page == 1) echo ' disabled';
            echo '" href="/',$_SESSION['page-params'][2],'?page=',$page-1,'"><i class="fas fa-chevron-left"></i></a>';


            for ($i = 0; $i < $count_pages; $i++){
                if ((in_array($page, range(3,$count_pages-2)) && in_array($i, range($page-3, $page+1))) ||
                    (in_array($page, range(1,2)) && in_array($i, range(0, 4)) ||
                    (in_array($page, range($count_pages-1,$count_pages)) && in_array($i, range($count_pages - 5, $count_pages - 1))))){
                    echo '<a class = "page-button num';
                    if ($i == $page - 1) echo ' active';
                    echo '" dataId="', $i + 1, /*'"href= "/', $_SESSION['page-params'][2], '?page=', $i + 1, */'">', $i + 1, '</a>';
                }
            }

            echo '<a class="page-button next';
            if ($page == $count_pages) echo ' disabled';
            echo '" href="/',$_SESSION['page-params'][2],'?page=',$page+1,'"><i class="fas fa-chevron-right"></i></a>';
        }

        function show_cart_item($cart_item,$product){
            $sum = $product[5] * $cart_item[3];
            echo '
                    <tr>
                        <td class="id">',$product[0],'</td>
                        <td class="img"><img src="',$product[3],'"></td>
                        <td class="name">',$product[1],'</td>
                        <td class="price">',$product[5],'</td>
                        <td class="quantity">
                            <div class="item-amount d-flex justify-content-start">
                                <div class="minus cart">-</div>
                                <input type="text" id="amount" name="amount" class="amount" min=1 value="',$cart_item[3],'">
                                <div class="plus cart">+</div>
                            </div>
                        </td>
                        <td class="sum">',$sum,'</td>
                        <td style="width:80px;"><span class="del"><i class="fas fa-times 3x"></i></span></td>
                    </tr>
                    
                    
                    ';



        }

        function show_cart($user_id){
            $cart_items = Cart::get_cart_items($user_id);
            $sum = Cart::get_sum($user_id);


            if (!$cart_items){
                echo '<div class = "empty-cart">Ваша корзина пуста. <a href = "/">Перейти на главную</a>.</div>';
            } else{
                echo '
                    <table class="cart table">
                        <thead>
                            <tr>
                                <th class="id">ID</th>
                                <th class="img">Фото</th>
                                <th class="name">Наименование</th>
                                <th class="price">Цена</th>
                                <th class="quantity">Количество</th>
                                <th class="sum">Сумма</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>                   
                ';
                foreach ($cart_items as $cart_item) {
                    $product = Products::get_cart_product($cart_item[2]);
                    self::show_cart_item($cart_item,$product);
                }
                echo '
            
                    </tbody>
                </table>
                <div class="d-flex justify-content-between">
                    <div class="clear-cart button">
                        Очистить корзину
                    </div>
                    <div class="cart-total">
                        Итого: <span><span>',$sum,'</span> ₽</span>
                    </div>
                </div>
                ';
            }






        }

        function show_cart_price($user_id){
            $sum = Cart::get_sum($user_id);
            if ($sum) {
                echo 'Сумма: <span class="total" ><span >',$sum,'</span> ₽</span>';
                }
            else {
                echo 'Корзина пуста';
            }
        }

        function show_cart_amount($user_id){
            $amount = Cart::get_amount($user_id);
            if ($amount) {
                echo $amount;
            }
            else {
                echo '0';
            }
        }


    }
