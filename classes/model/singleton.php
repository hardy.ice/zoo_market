<?php

    require_once $_SERVER["DOCUMENT_ROOT"]."/config.php";



    class DB {
        static private $db;

        private function __construct() {}

        private function __clone() {}

        static function getInstance() {
            if(empty(self::$db)) {
                self::$db = new Mysqli(C_HOST, C_USER, C_PASS, C_DATABASE);
            }
            return self::$db;
        }
    }

    class DBQuery extends DB {

        public static  function  query($sql){
            $result = true;
            $db=parent::getInstance();
            $result_ob = $db->query($sql);
            if (!(is_bool($result_ob))){
                $result = $result_ob->fetch_all();
            }
           if(empty($result_ob)){
                $result = false;
            }
            //echo $db->error();

            return $result;
        }
    }


    //var_dump(DBQuery::query("SELECT * FROM users"));