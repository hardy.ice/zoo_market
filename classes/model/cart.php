<?php

    require_once $_SERVER["DOCUMENT_ROOT"]."/classes/model/singleton.php";

    class Cart{

        public static function get_cart_items($user_id){
            $res = DBQuery::query("SELECT * FROM `cart` where `user_id` = '$user_id'");
            if (!$res){
                $res = false;
            }
            return $res;
        }
        public static function get_cart_item($user_id,$product_id){
            $res = DBQuery::query("SELECT * FROM `cart` WHERE `user_id` = '$user_id' AND `product_id` = '$product_id'")[0];
            if (!$res){
                $res = false;
            }
            return $res;
        }

        public static function update_cart_item($user_id,$product_id,$amount){
            $res = DBQuery::query("UPDATE `cart` SET `product_quantity` = '$amount' WHERE `user_id` = '$user_id' AND `product_id` = '$product_id'");
            if (!$res){
                $res = false;
            }
            return $res;
        }

        public static function add_item_to_cart($user_id,$product_id,$amount){
            $product = self::get_cart_item($user_id,$product_id);
            var_dump($product);
            if ($product){
                $amount += $product[3];
                return DBQuery::query("UPDATE `cart` SET `product_quantity` = '$amount' WHERE `user_id` = '$user_id' AND `product_id` = '$product_id'");
            }
            return DBQuery::query("INSERT INTO `cart` (`user_id`, `product_id`, `product_quantity`) VALUES ('$user_id', '$product_id', '$amount')");
        }


        public static function clear_cart($user_id){
            return DBQuery::query("DELETE FROM `cart` WHERE `user_id` = '$user_id'");
        }

        public static function get_count_items($user_id){
            $res = DBQuery::query("SELECT count(*) FROM `cart` where `user_id` = '$user_id'");
            if (!$res){
                $res = false;
            }
            return $res;
        }

        public static function get_sum($user_id){
            $sum = 0;
            $items = DBQuery::query("SELECT * FROM `cart` where `user_id` = '$user_id'");
            foreach ($items as $item){
                $price = (int) Products::get_price($item[2]);
                $sum = $sum + ($price * $item[3]);
            }
            return $sum;
        }

        public static function delete_item($user_id,$product_id){
            return DBQuery::query("DELETE FROM `cart` WHERE `user_id` = '$user_id' AND `product_id` = '$product_id'");
        }

        public static function get_amount($user_id){
            $amount = 0;
            $items = DBQuery::query("SELECT * FROM `cart` where `user_id` = '$user_id'");
            foreach ($items as $item){
                $amount = $amount + $item[3];
            }
            return $amount;
        }







    }