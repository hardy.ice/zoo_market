<?php

    require_once $_SERVER["DOCUMENT_ROOT"]."/classes/model/singleton.php";

    class ParseClass{

        public static function load($file_name){
            if (file_exists($file_name)) {
                return $xml = simplexml_load_file($file_name);
            } else {
                exit('Не удалось открыть файл '.$file_name);
            }
        }



        public static function add_section($value){
            $section = array (
                'id' => $value->id->__toString(),
                'name' => $value->name->__toString(),
            );
            if ($section['id']) {
                return DBQuery::query("INSERT INTO `sections` VALUES ($section[id], '$section[name]') ON DUPLICATE KEY UPDATE `name` = '$section[name]';");
            }
            else {
                return DBQuery::query("INSERT INTO `sections` (`name`) VALUES ('$section[name]') ON DUPLICATE KEY UPDATE `name` = '$section[name]';");
            }
        }

        public static function add_char($value){
            $char = array (
                'id' => $value->id->__toString(),
                'name' => $value->name->__toString(),
            );
            if ($char['id']) {
                return DBQuery::query("INSERT INTO `chars_dictionary` VALUES ($char[id], '$char[name]') ON DUPLICATE KEY UPDATE `name` = '$char[name]';");
            }
            else {
                return DBQuery::query("INSERT INTO `chars_dictionary` (`name`) VALUES ('$char[name]') ON DUPLICATE KEY UPDATE `name` = '$char[name]';");
            }
        }

        public static function add_product($value){
            $product = array (
                'id' => $value->id->__toString(),
                'name' => $value->name->__toString(),
                'id_section' => $value->id_section->__toString(),
                'image' => $value->image->__toString(),
                'description' => ltrim(rtrim($value->description->asXML(),"</description>"),"<description>"),
                'price' => rand(100,1000),
            );


            if (!$product['id']){
                $last_id = DBQuery::query("SELECT MAX(`id`) FROM `products`");
                $product['id'] = $last_id[0][0] + 1;
            }
            DBQuery::query("INSERT INTO `products` VALUES ($product[id], '$product[name]', $product[id_section], '$product[image]', '$product[description]', '$product[price]');");

            foreach ($value->characteristics->characteristic as $item) {
                self::add_product_chars($product['id'],$item);
            }
            return true;

        }

        public static function add_product_chars($product_id,$item){
            $char = array (
                'id' => $item->id->__toString(),
                'value' => $item->value->__toString(),
            );
            return DBQuery::query("INSERT INTO `product_chars` (`product_id`, `char_id`, `char_value`) VALUES ($product_id, '$char[id]', '$char[value]')");
        }


    }