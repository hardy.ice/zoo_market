<?php

    require_once $_SERVER["DOCUMENT_ROOT"]."/classes/model/sections.php";
    require_once $_SERVER["DOCUMENT_ROOT"]."/classes/model/products.php";
    require_once $_SERVER["DOCUMENT_ROOT"]."/classes/model/chars.php";



    class Breadcrumbs
    {




        function get_breadcrumbs(){
            if (isset($_SESSION['page-params'][3]))
                $product = Products::get_product($_SESSION['page-params'][3]);
            //var_dump($_SESSION['page-params'][3]);
            //echo '<br>';
            //echo '<br>';
            $a = array (
                'dogs' => 'Товары для собак',
                'cats' => 'Товары для кошек',
                'rodents' => 'Товары для грузынов',
                'birds' => 'Товары для грузынов',
                'cart' => 'Корзина',
                '' => 'Главная',
                'dogs?page=[0-9]+' => 'Товары для собак',
                'cats?page=[0-9]+' => 'Товары для кошек',
                'rodents?page=[0-9]+' => 'Товары для грузынов',
                'birds?page=[0-9]+' => 'Товары для грузынов',
                'cart?page=[0-9]+' => 'Корзина',
            );
            if (isset($product))
                $a['[0-9]+'] = $product[1];
            $cur_url = $_SERVER['REQUEST_URI'];
            $urls = explode('/', $cur_url);

            $crumbs = array();
            if (!empty($urls) && $cur_url != '/') {
                foreach ($urls as $key => $value) {
                    $prev_urls = array();
                    for ($i = 0; $i <= $key; $i++) {
                        $prev_urls[] = $urls[$i];
                        if ($key == count($urls) - 1) {
                            $crumbs[$key]['url'] = '';
                        } elseif (!empty($prev_urls)) {
                            $crumbs[$key]['url'] = count($prev_urls) > 1 ? implode('/', $prev_urls) : '/';
                        }
                    }
                }
                for ($i = 0; $i <= (count($prev_urls) - 1); $i++) {
                    foreach ($a as $key => $value) {
                        //var_dump($key);
                        //echo '<br>';
                        $match = array();
                        $m = preg_match('@^' . $key . '$@Uu', $prev_urls[$i], $match);
                        if ($m > 0) {
                            $crumbs[$i]['text'] = $value;
                            break;
                        }
                    }
                }
            }
        return $crumbs;
        }
    }