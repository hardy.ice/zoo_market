<?php

    require_once $_SERVER["DOCUMENT_ROOT"]."/classes/model/singleton.php";

    class Products{



        public static function get_count_products($section_id){
            return (DBQuery::query("SELECT COUNT(*) FROM `products` WHERE `section_id` = $section_id"))[0][0];
        }

        public static function get_20_products($section_id, $start){
            $res = DBQuery::query("SELECT * FROM `products` where `section_id` = $section_id ORDER BY `id` LIMIT $start, 20");
            if (!$res){
                $res = false;
            }
            return $res;
        }

        public static function get_all_products($section_id){
            $res = DBQuery::query("SELECT * FROM `products` where `section_id` = $section_id");
            if (!$res){
                $res = false;
            }
            return $res;
        }

        public static function get_similar_products($section_id){
            $res = DBQuery::query("SELECT * FROM `products` where `section_id` = $section_id LIMIT 5");
            if (!$res){
                $res = false;
            }
            return $res;
        }


        public static function get_product_chars($product_id){
            $temp = DBQuery::query("SELECT * FROM `product_chars` where `product_id` = $product_id");
            if (!$temp){
                $res = false;
            } else {
                foreach ($temp as $char){

                    //var_dump(Chars::get_char($char[2]));
                    $res[] = [0 => Chars::get_char($char[2]),
                              1 => $char[3]];
                }
            }
            return $res;
        }


        public static function get_product($id){
            $res = DBQuery::query("SELECT * FROM `products` where `id` = $id");
            $a = self::get_product_chars($id);
            $res = array_merge($res[0],$a);
            if (!$res){
                $res = false;
            }
            return $res;
        }

        public static function get_cart_product($id){
            $res = DBQuery::query("SELECT * FROM `products` where `id` = $id");
            if (!$res){
                $res = false;
            }
            return $res[0];
        }

        public static function get_price($id){
            $res = DBQuery::query("SELECT `price` FROM `products` where `id` = $id")[0][0];
            if (!$res){
                $res = false;
            }
            return $res;
        }



    }
