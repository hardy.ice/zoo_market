<?php

    require_once $_SERVER["DOCUMENT_ROOT"]."/classes/view/view.php";
    require_once $_SERVER["DOCUMENT_ROOT"]."/classes/model/products.php";
    require_once $_SERVER["DOCUMENT_ROOT"]."/classes/model/cart.php";


    session_start();


//var_dump($_POST);
//var_dump($_FILES);
//var_dump('jjj');


    switch ($_POST["command"]) {

        case "add-to-cart":
            {
                echo Cart::add_item_to_cart($_POST['user-id'],$_POST['item-id'],$_POST['amount']);
                break;
            }
        case "create-cookie":
            {
                echo md5(time());
                break;
            }
        case "show-cart":
            {
                View::show_cart($_POST['user-id']);
                break;
            }
        case "clear-cart":
            {
                Cart::clear_cart($_POST['user-id']);
                View::show_cart($_POST['user-id']);
                break;
            }
        case "update-item-amount":
            {
                echo Cart::update_cart_item($_POST['user-id'],$_POST['product-id'],$_POST['amount']);
                break;
            }
        case "view-cart-total":
            {
                echo View::show_cart_price($_POST['user-id']);
                break;
            }
        case "delete-item":
            {
                Cart::delete_item($_POST['user-id'],$_POST['product-id']);
                View::show_cart($_POST['user-id']);
                break;
            }
        case "view-cart-amount":
            {
                echo View::show_cart_amount($_POST['user-id']);
                break;
            }
    }

