$(document).ready(function() {

//функция получения куки по имени
    function getCookie(name) {
        var matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    }

//создание юзер-ид в куки
    if (!getCookie('user_id')){
        var comm = [];
        comm.push({name:"command",value:"create-cookie"});
        $.post("/classes/controller/controller.php", comm)
            .done (function (result) {
                if (result){
                    document.cookie = "user_id=" + result;
                }
                else{
                    alert ("Действие не выполнено.")
                }
            })
            .fail (function () {
                alert("Ошибка доступа к серверу");
            });
    }



//если страница - корзина, то выводим табличу корзины
    if (location.pathname == '/cart'){
        var comm = [];
        comm.push({name:"command",value:"show-cart"});
        comm.push({name:"user-id",value:getCookie('user_id')});
        //console.log(comm);
        $.post("/classes/controller/controller.php", comm)
            .done (function (result) {
                if (result){
                    $(".main-content").html(result);
                }
                else{
                    alert ("Действие не выполнено.")
                }
            })
            .fail (function () {
                alert("Ошибка доступа к серверу");
            });
    }


//обновление суммы в корзине при любом клике
    function cart_total_price_updating() {
        var comm = [];
        comm.push({name:'command',value:'view-cart-total'});
        comm.push({name:"user-id",value:getCookie('user_id')});
        $.post("/classes/controller/controller.php", comm)
            .done(function (result) {
                if (result) {
                    $("a.shopping-cart div.cart-info div.total-cart").html(result);
                } else {
                    alert("Действие не выполнено.")
                }
            })
            .fail(function () {
                alert("Ошибка доступа к серверу");
            });
    }

    cart_total_price_updating();
    $("body").on("click",cart_total_price_updating);

//обновление количества товаров в корзине при любом клике
    function cart_amount_updating() {
        var comm = [];
        comm.push({name:'command',value:'view-cart-amount'});
        comm.push({name:"user-id",value:getCookie('user_id')});
        $.post("/classes/controller/controller.php", comm)
            .done(function (result) {
                if (result) {
                    $("div.cart-amount").html(result);
                } else {
                    alert("Действие не выполнено.")
                }
            })
            .fail(function () {
                alert("Ошибка доступа к серверу");
            });
    }

    cart_amount_updating();
    $("body").on("click",cart_amount_updating);

//работа кнопок + и - в поле для выбора количества товара
    $('.main-content').on("click",".item-amount .minus",function() {
        $a = parseInt($(this).parent().find(".amount").val());
        console.log($a);
        if ($a > 1)
            $(this).parent().find(".amount").val($a - 1);
        else
            $(this).addClass("disabled");
    });

    $('.main-content').on("click",".item-amount .plus",function() {
        $a = parseInt($(this).parent().find(".amount").val());
        $(this).parent().find(".amount").val($a + 1);
    });


//кнопки + и - в корзине меняют количество товара в БД
    $('.main-content').on("click input",".item-amount .minus.cart, .item-amount .plus.cart, .item-amount .amount",function() {
        var comm = [];
        comm.push({name:"command",value:"update-item-amount"});
        comm.push({name:"user-id",value:getCookie('user_id')});
        comm.push({name: "product-id",value: $(this).parent().parent().parent().find("td.id").html()});
        comm.push({name: "amount",value: parseInt($(this).parent().find(".amount").val())});
        $.post("/classes/controller/controller.php", comm)
            .done (function (result) {
                if (result){

                }
                else{
                    alert ("Действие не выполнено.")
                }
            })
            .fail (function () {
                alert("Ошибка доступа к серверу");
            });
    });

//пересчет суммы по товару и общей суммы
    $('.main-content').on("click input",".item-amount .minus.cart, .item-amount .plus.cart, .item-amount .amount",function() {
        $(this).parent().parent().parent().find(".sum").html( $(this).parent().find(".amount").val() * $(this).parent().parent().parent().find(".price").html() );
        sum = 0;
        $("td.sum").each(function(index, value) {
            sum += parseInt(value.innerHTML);
        });
        sum = sum + '';
        sum = sum.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
        $(".cart-total span span").html(sum);
        console.log($("a.shopping_cart span.total span"));
        $("a.shopping_cart span.total span").html(sum);

    });



//смена класса активных кнопок пагинации
    $(".page-button.num").on("click",function(){
        console.log($(this));
        $(this).parent().parent().find(".active").toggleClass("active");
        $(this).toggleClass("active");
    });

//запрос данных текущей страницы секции
    $(".page-button.num").on("click",function(){
        window.location.href = location.protocol + '//' + location.host + location.pathname + '?page=' + $(this).attr("dataId");
    });

//добавление товара в корзину
    $(".add-to-cart.button").on("click",function(){
        //console.log('ddd');
        var comm = [];
        comm.push({name:"command",value:"add-to-cart"});
        comm.push({name:"item-id",value: $(this).attr("dataId")});
        comm.push({name:"amount",value:1});
        comm.push({name:"user-id",value:getCookie('user_id')});
        if ($("#amount").val()){
            comm[2] = ({name:"amount",value: parseInt($("#amount").val())});
        }
        //console.log(comm);
        $.post("/classes/controller/controller.php", comm)
            .done (function (result) {
                console.log(result);
                if (result){
                    alert("Товар добавлен в корзину")
                }
                else{
                    alert ("Действие не выполнено.")
                }
            })
            .fail (function () {
                alert("Ошибка доступа к серверу");
            });
    });


//очистка корзины
    $('.main-content').on("click",".clear-cart.button",function(){
        var comm = [];
        comm.push({name:"command",value:"clear-cart"});
        comm.push({name:"user-id",value:getCookie('user_id')});
        console.log(comm);
        $.post("/classes/controller/controller.php", comm)
            .done (function (result) {
                //console.log(result);
                if (result){
                    $(".main-content").html(result);
                }
                else{
                    alert ("Действие не выполнено.")
                }
            })
            .fail (function () {
                alert("Ошибка доступа к серверу");
            });
    });

//удаление товара из корзины
    $('.main-content').on("click","span.del",function(){
        var comm = [];
        comm.push({name:"command",value:"delete-item"});
        comm.push({name:"user-id",value:getCookie('user_id')});
        comm.push({name: "product-id",value: $(this).parent().parent().find("td.id").html()});
        console.log(comm);
        $.post("/classes/controller/controller.php", comm)
            .done (function (result) {
                if (result){
                    $(".main-content").html(result);
                }
                else{
                    alert ("Действие не выполнено.")
                }
            })
            .fail (function () {
                alert("Ошибка доступа к серверу");
            });
    });

/*
    //функция парсинга .xml
    $(".qqq").on("click",function() {
        $(".modal-bg").addClass("hidden");
    });
    $.ajax({
        url: './parse.php',
        cache:false,
        processData: false,
        contentType: false,
        type: 'POST',
        success: function(respond){
            //alert('успех');
            console.log(respond)},
        error: function(){alert('неудача')}
    });
*/



});