

        <div class="main-header">
            Каталог
        </div>
        <div class="main-content">
            <div class="row">
                <div class="col catalog-section">
                    <a href="/dogs">
                        <img src="/img/catalog/dogs.png">
                        <div class="label"></div>
                        <div class="label-text">Корм собакам</div>
                        <div class="more">Подробнее</div>
                    </a>
                </div>
                <div class="col catalog-section">
                    <a href="/cats">
                        <img src="/img/catalog/cats.png">
                        <div class="label"></div>
                        <div class="label-text">Корм кошкам</div>
                        <div class="more">Подробнее</div>
                    </a>
                </div>
                <div class="col catalog-section">
                    <a href="/rodents">
                        <img src="/img/catalog/rodents.png">
                        <div class="label"></div>
                        <div class="label-text">Корм грызунам</div>
                        <div class="more">Подробнее</div>
                    </a>
                </div>
                <div class="col catalog-section">
                    <a href="/birds">
                        <img src="/img/catalog/birds.png">
                        <div class="label"></div>
                        <div class="label-text">Корм птицам</div>
                        <div class="more">Подробнее</div>
                    </a>
                </div>
            </div>




        </div>


