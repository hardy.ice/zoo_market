<!DOCTYPE HTML>
<html lasite-headerng="en">
    <head>
        <meta charset="utf-8">
        <title>Тузики-Мурзики — зоомаркет</title>

        <link rel="stylesheet" href="/css/reset.css">
        <link rel="stylesheet" href="/css/bootstrap.min.css">
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,700,700i&amp;subset=cyrillic" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        <!--link rel="stylesheet" href="./css/nice-select.css"-->
        <link rel="stylesheet" href="/css/jquery-ui.min.css">
        <!--link rel="stylesheet" href="./css/hamburger-animation.css"-->

        <link rel="stylesheet" href="/css/style.css">
        <link rel="stylesheet" href="/css/adaptive.css">
        <link href="/img/favicon.ico" rel="shortcut icon" type="image/x-icon">

        <meta name="keywords" content="">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, shrink-to-fit=no">

    </head>
    <body>

        <header>
            <div class="wrap d-flex justify-content-between">
                <div class="header_logo"><a href="/"><img src="/img/header_logo.png" alt="tuziki-murziki-logo"></a></div>
                <a href="/cart" class="shopping-cart d-flex">
                    <div class="cart-info">
                        <div class="uppercase">Корзина</div>
                        <div class="total-cart"></div>
                    </div>
                    <div class="cart-pic">
                        <img src="/img/cart.png" alt="cart">
                        <div class="cart-amount"></div>
                    </div>
                </a>
            </div>
        </header>
        <nav>
            <div class="wrap">
                <div role="navigation">
                    <ul class="nav">
                        <li class="nav-item"><a class="nav-link" href="/">Главная</a></li>
                        <li class="nav-item"><a class="nav-link" href="/dogs">Корм собакам</a></li>
                        <li class="nav-item"><a class="nav-link" href="/cats">Корм кошкам</a></li>
                        <li class="nav-item"><a class="nav-link" href="/rodents">Корм грызунам</a></li>
                        <li class="nav-item"><a class="nav-link" href="/birds">Корм птицам</a></li>
                    </ul>
                </div>
            </div>
        </nav>

        <main>
            <div class="wrap">

                <div class="breadcrumbs-nav">
                    <?php require_once $_SERVER["DOCUMENT_ROOT"]."/classes/view/view.php";
                    View::show_breadcrumbs($GLOBALS['page-params'][1]);?>
                </div>

