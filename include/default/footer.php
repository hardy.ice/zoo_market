
            </div>
        </main>

        <footer class="row align-items-center">
            <div class="wrap row align-items-center d-flex justify-content-between">
                <div class="col"><img class="qqq" src="/img/footer_logo.png" alt="tuziki-murziki-logo"></div>
                <div class="col">Каталог:</div>
                <div class="col"><a href="/dogs">Собакам</a></div>
                <div class="col"><a href="/cats">Кошкам</a></div>
                <div class="col"><a href="rodents">Грызунам</a></div>
                <div class="col"><a href="birds">Птицам</a></div>
            </div>
        </footer>



        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <script src="/js/jquery.js"></script>
        <script src="/js/jquery-ui.js"></script>
        <!--script src="./js/jquery.maskedinput.min.js"></script-->
        <!--script src="/js/jquery.nice-select.js"></script-->
        <!--script src="/js/datepicker-ru.js"></script-->
        <script src="/js/jquery.validate.min.js"></script>
        <script src="/js/script.js"></script>
    </body>
</html>