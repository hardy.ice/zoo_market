<?php



    class uSitemap {
        public $title = '';
        public $params = null;
        public $classname = '';
        public $data = null;

        public $request_uri = '';
        public $url_info = array();

        public $found = false;

        function __construct() {
            $this->mapClassName();
        }

        function mapClassName() {

            $this->classname = '';
            $this->title = '';
            $this->params = null;

            $map = array (
                '_404' => '404.php',
                '/' => 'main.php',
                '/dogs' => 'section.php,99',
                '/cats' => 'section.php,100',
                '/rodents' => 'section.php,101',
                '/birds' => 'section.php,102',
                '/dogs(/[0-9]+)?' => 'item.php,99',
                '/cats(/[0-9]+)?' => 'item.php,100',
                '/rodents(/[0-9]+)?' => 'item.php,101',
                '/birds(/[0-9]+)?' => 'item.php,102',
                '/cart' => 'cart.php',
            );

            session_start();


            unset($_SESSION['page-params']);
            $_SESSION['page-params'][0] = '404.php';

            $this->request_uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
            $this->url_info = parse_url($this->request_uri);
            $uri = urldecode($this->url_info['path']);
            foreach ($map as $term => $dd) {
                $match = array();
                $i = preg_match('@^'.$term.'$@Uu', $uri, $match);
                if ($i > 0) {
                    // Get class name and main title part
                    $m = explode(',', $dd);
                    $data = array(
                        'classname' => isset($m[0])?strtolower(trim($m[0])):'',
                        'title' => isset($m[1])?trim($m[1]):'',
                        'params' => $match,
                    );
                    $exp = explode("/",trim($data['params'][0],"/"));
                    $_SESSION['page-params'][0]=$data['classname'];
                    if (isset($data['title']))
                        $_SESSION['page-params'][1]=$data['title'];
                    if (isset($exp[0]))
                        $_SESSION['page-params'][2]=$exp[0];
                    if (!empty($exp[1]))
                        $_SESSION['page-params'][3]=$exp[1];
                    //echo '<br><br>';
                    break;
                }
            }
            return $this->classname;
        }
    }

    $sm = new uSitemap();



    require('./classes/view/page.php'); // Подключаем файл

    // P.S. Внутри подключённого файла Вы можете использовать параметры запроса,
    // которые хранятся в свойстве $sm->params